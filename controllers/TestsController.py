from models import RallyModel, RobotModel, TempestModel, TestModel
import json
import os
from configuration import config as cnf


class TestsController:

    def launch_test_ajax(self, test_suite, test_type, test_module, test_name):
        test = None
        response = "Test Suite Undefined!"

        if test_suite == "rally":
            rally = RallyModel.Rally()
            test = rally.launch_test(test_module, test_name)
        elif test_suite == "robot":
            robot = RobotModel.Robot()
            test = robot.launch_test(test_module, test_type, test_name)
        elif test_suite == "tempest":
            tempest = TempestModel.Tempest()
            test = tempest.launch_test(test_module, test_name)

        if test is not None:
            test.create_tp_database()
            test.insertTest()
            response = json.dumps(test, default=lambda o: o.__dict__)

        return response

    def get_last_test_ajax(self, test_suite, test_type, test_module, test_name):
        last_test = TestModel.TestModel(test_suite=test_suite, test_type=test_type, test_module=test_module, test_name=test_name)
        last_test.create_tp_database()
        last_test.getTest()
        if test_suite == "rally":
            with open(os.path.join(cnf.BASE_DIR, "templates", "iframe_output_result.html"), "w") as f:
                f.write(last_test.last_test_result)
            return json.dumps(last_test, default=lambda o: o.__dict__)
        elif test_suite == "robot":
            print last_test.report_html
            with open(os.path.join(cnf.BASE_DIR, "templates", "report.html"), "w") as f:
                f.write(last_test.report_html)
            with open(os.path.join(cnf.BASE_DIR, "templates", "log.html"), "w") as f:
                f.write(last_test.log_html)
            return json.dumps(last_test, default=lambda o: o.__dict__)
        if test_suite == "tempest":
            with open(os.path.join(cnf.BASE_DIR, "templates", "iframe_output_result.html"), "w") as f:
                f.write(last_test.last_test_result)
            return json.dumps(last_test, default=lambda o: o.__dict__)

    def get_rally_installed_modules(self):
        rally = RallyModel.Rally()
        return rally.get_installed_modules()
