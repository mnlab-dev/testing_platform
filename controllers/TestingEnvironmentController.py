from models import RallyModel, RobotModel, TestingEnvironmentModel
import json


class TestingEnvironmentController:

    def handle_install_testing_environment(self):
        rally = RallyModel.Rally()
        robot = RobotModel.Robot()

        # TODO handle result stdout, stderr
        rally.install()
        robot.install()

        # TODO fix not having to run twice
        result = rally.install()
        print result
        robot.install()
        # result = robot.install()

        response = json.dumps({"result": result})

        return response

    def handle_check_testing_environment(self):
        rally = RallyModel.Rally()
        robot = RobotModel.Robot()

        # TODO handle result stdout, stderr
        result = robot.check()
        print result
        result = rally.check()
        print result

        response = json.dumps({"result": result})

        return response

    def handle_clean_testing_environment(self):

        testingEnvironment = TestingEnvironmentModel.TestingEnvironment()

        result = testingEnvironment.clean()

        response = json.dumps({"result": result})

        return response
