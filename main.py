#!/usr/bin/python
#  -*- coding: utf8 -*-
from flask import Flask, request, render_template
from controllers.TestingEnvironmentController import TestingEnvironmentController
from controllers.TestsController import TestsController
import os

app = Flask(__name__)


# HTTP routes
@app.route('/')
def index_html(): return render_template('index.html')


@app.route('/header')
def header_html(): return render_template('header.html')


@app.route('/sidebar')
def sidebar_html(): return render_template('sidebar.html')


@app.route('/testing_environment')
def testing_environment_html(): return render_template('testing_environment.html')


@app.route('/tests')
def tests_html(): return render_template('tests.html')


@app.route('/iframe_output_result')
def iframe_output_result_html(): return render_template('iframe_output_result.html')


@app.route('/report.html')
def iframe_report(): return render_template('report.html')


@app.route('/log.html')
def iframe_log(): return render_template('log.html')


# Testing Environment routes
@app.route('/installTestingEnvironment', methods=['POST'])
def install_testing_environment(): return TestingEnvironmentController().handle_install_testing_environment()


@app.route('/checkTestingEnvironment', methods=['POST'])
def check_testing_environment(): return TestingEnvironmentController().handle_check_testing_environment()


@app.route('/cleanTestingEnvironment', methods=['POST'])
def clean_testing_environment(): return TestingEnvironmentController().handle_clean_testing_environment()


# Tests routes
@app.route('/launchTestAjax', methods=['POST'])
def launch_test_ajax():
    test_suite = request.form["test_suite"]
    test_type = request.form["test_type"]
    test_module = request.form["test_module"]
    test_name = request.form["test_name"]
    return TestsController().launch_test_ajax(test_suite=test_suite, test_type=test_type, test_module=test_module, test_name=test_name)


# Get last result
@app.route('/getLastTestResult', methods=['POST'])
def get_last_test_result():
    test_suite = request.form["test_suite"]
    test_type = request.form["test_type"]
    test_module = request.form["test_module"]
    test_name = request.form["test_name"]
    return TestsController().get_last_test_ajax(test_suite=test_suite, test_type=test_type, test_module=test_module, test_name=test_name)

# Get Rally module installation state
@app.route("/getRallyInstalledModules", methods=['POST'])
def get_rally_installed_modules():
    return TestsController().get_rally_installed_modules()

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8081, threaded=True)
