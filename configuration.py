import os
import configparser
import getpass


class config:

    # --------------------------  Global Variables  -----------------------------
    # Base Directory
    BASE_DIR = os.path.dirname(__file__)
    BASE_USER_DIR = os.path.join("/home", getpass.getuser())

    # Configuration File Directory
    CONF_FILE_DIR = os.path.join("aio.conf")

    config = configparser.ConfigParser()
    config.read(CONF_FILE_DIR)

    # -------------------------- Rally -------------------------------------------
    # Installation Script Directory
    RALLY_REPO_DIR = os.path.join(BASE_DIR, "repository")

    # Rally Installation Directory
    RALLY_INSTALLATION_DIR = os.path.join("/home", getpass.getuser(), ".rally/")

    # Rally Deployment Name
    DEPLOYMENT_NAME = config['RALLY']['deployment_name']

    # Rally tests directory
    RALLY_TESTS_DIR = os.path.join("/home", getpass.getuser(), "rally", "samples", "tasks", "scenarios")
    RALLY_HEAT_TEMPLATE_DIR = os.path.join(RALLY_TESTS_DIR, "heat")
    RALLY_DATABASE_DIR = os.path.join("/home", getpass.getuser(), "rally", "database")

    # Rally tests directory
    RALLY_DATABASE_NAME = config['RALLY']['database_name']

    # -------------------------- Robot -------------------------------------------
    # Installation Script Directory
    ROBOT_REPO_DIR = os.path.join(BASE_DIR, "repository")

    # -------------------------- Root --------------------------------------------
    # Root password
    ROOT_PASSWORD = config['ROOT']['password']

    # -------------------------- Database ----------------------------------------
    # Database Name
    DATABASE_NAME = config['DATABASE']['name']
    # Database Directory
    DATABASE_DIRECTORY = os.path.join(BASE_DIR, "db")
    # DATABASE_DIRECTORY = "/home/localadmin/testing_platform_py/db/"

    # ENVIRONMENTAL VARIABLES
    OS_USERNAME    = config['OPENSTACK']['OS_USERNAME']
    OS_PASSWORD    = config['OPENSTACK']['OS_PASSWORD']
    OS_TENANT_NAME = config['OPENSTACK']['OS_TENANT_NAME']
    OS_AUTH_URL    = config['OPENSTACK']['OS_AUTH_URL']

    OS_PUBLIC_NET_NAME = config['OPENSTACK']['OS_PUBLIC_NET_NAME']
    OS_CIRROS_IMG_NAME = config['OPENSTACK']['OS_CIRROS_IMG_NAME']

    # OPENDAYLIGHT
    ODL_IP            = config['OPENDAYLIGHT']['ODL_IP']
    ODL_BASE_TEST_DIR = os.path.join(BASE_DIR, "opendaylight_tests", "integration-test", "csit", "suites")