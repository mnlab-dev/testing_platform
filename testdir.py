import os, sys
from configuration import config
import json


def replace_floating_network(floating_network_name, scenario_file):
    with open(scenario_file, 'r+') as f:
        try:
            clean_json_string = ""
            latest_new_line_char = 0
            # in case json has template-like "{% %}" lines
            for line in f:
                if not line.startswith("{%"):
                    clean_json_string += line + "\n"
                else:
                    latest_new_line_char += line.find("\n") + 1

            data = json.loads(clean_json_string)
            test_name = data.keys()[0]
            if 'args' in data[test_name][0]:
                if 'floating_network' in data[test_name][0]["args"]:
                    print "FILE MODIFIED: " + scenario_file
                    data[test_name][0]["args"]["floating_network"] = floating_network_name
                    f.seek(latest_new_line_char)
                    json.dump(data, f, indent=4)
                    f.truncate()
            else:
                print "floating_network keyname not found"
        except Exception as e:
            print scenario_file
            print e
            print sys.exc_traceback.tb_lineno


cnf = config
outer_path = cnf.RALLY_TESTS_DIR
# outer_path = "/root"
print outer_path
f = []
for dirname in os.listdir(outer_path):
    if os.path.isdir(os.path.join(outer_path, dirname)):
        for filename in os.listdir(os.path.join(outer_path, dirname)):
            if os.path.isfile(os.path.join(outer_path, dirname, filename)) and filename.split(".")[-1] == "json":
                replace_floating_network("ext-net", os.path.join(outer_path, dirname, filename))
