// Create Floating IPs And Delete
function neutronCreateFloatingIPsAndDeleteAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateFloatingIPsAndDeleteAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And Delete Networks
function neutronCreateAndDeleteNetworksAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndDeleteNetworksAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And List Networks
function neutronCreateAndListNetworksAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndListNetworksAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And Delete Pools
function neutronCreateAndDeletePoolsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndDeletePoolsAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And Delete Routers
function neutronCreateAndDeleteRoutersAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndDeleteRoutersAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And Delete Subnets
function neutronCreateAndDeleteSubnetsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndDeleteSubnetsAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And List Floating IPs
function neutronCreateAndListFloatingIpsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndListFloatingIpsAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}


// Create And List Subnets
function neutronCreateAndListSubnetsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndListSubnetsAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Create And Update Networks
function neutronCreateAndUpdateNetworksAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndUpdateNetworksAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}


// Create And Update Pools
function neutronCreateAndUpdatePoolsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/neutronCreateAndUpdatePoolsAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}


// Create And Update Routers
function neutronCreateAndUpdateRoutersAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaCreateAndListNetworksAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}


// Create And Update Subnets
function neutronCreateAndUpdateSubnetsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaCreateAndListNetworksAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}