// List Hypervisors
function novaListHypervisorsAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaListHypervisorsAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// List Images
function novaListImagesAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaListImagesAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// List Servers
function novaListServersAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaListServersAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Suspend and resume
function novaSuspendAndResumeAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaSuspendAndResumeAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Boot
function novaBootAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaBootAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Boot an list
function novaBootAndListAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaBootAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Boot And Associate Floating Ip
function novaBootAndAssociateFloatingIpAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaBootAndAssociateFloatingIpAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}


// Boot And Delete
function novaBootAndDeleteAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaBootAndDeleteAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Suspend And Resume
function novaSuspendAndResumeAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaSuspendAndResumeAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}


// Create And List Networks
function novaCreateAndListNetworksAjax(thisButton, callback) {
    $.ajax({
        type: 'POST',
        url:  '/novaCreateAndListNetworksAjax',
        dataType: "json",
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}