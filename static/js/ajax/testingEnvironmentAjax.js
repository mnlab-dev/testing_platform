// Install Rally
function installTestingEnvironmentAjax(thisButton, callback) {
$.ajax({
    type: 'POST',
    url:  '/installTestingEnvironment',
    dataType: "json",
    success: function(data)
    {
        if( callback ) callback(data);
    },
    error: function(err)
    {
        if (thisButton !== null) {
            thisButton.parent().parent().next().children('.alert').addClass('hidden');
            thisButton.parent().parent().next().children('.alert-danger').removeClass('hidden');
            console.log("Παρουσιάστηκε σφάλμα!");
        }
    }
});
}

// Check Rally
function checkTestingEnvironmentAjax(thisButton, callback) {
$.ajax({
    type: 'POST',
    url:  '/checkTestingEnvironment',
    dataType: "json",
    success: function(data)
    {
        if( callback ) callback(data);
    },
    error: function(err)
    {
        if (thisButton !== null) {
            thisButton.parent().parent().next().children('.alert').addClass('hidden');
            thisButton.parent().parent().next().children('.alert-danger').removeClass('hidden');
            console.log("Παρουσιάστηκε σφάλμα!");
        }

    }
});
}

// Clean Rally
function cleanTestingEnvironmentAjax(thisButton, callback) {
$.ajax({
    type: 'POST',
    url:  '/cleanTestingEnvironment',
    dataType: "json",
    success: function(data)
    {
        if( callback ) callback(data);
    },
    error: function(err)
    {
        if (thisButton !== null) {
            thisButton.parent().parent().next().children('.alert').addClass('hidden');
            thisButton.parent().parent().next().children('.alert-danger').removeClass('hidden');
            console.log("Παρουσιάστηκε σφάλμα!");
        }
    }
});
}