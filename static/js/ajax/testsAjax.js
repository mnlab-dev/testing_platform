// Launch Test
function launchTestAjax(thisButton, callback) {
    thisSuite = thisButton.siblings("button").attr("suite")
    thisType = thisButton.siblings("button").attr("type")
    thisModule = thisButton.siblings("button").attr("category")
    thisTest = thisButton.siblings("button").attr("test")
    $.ajax({
        type: 'POST',
        url:  '/launchTestAjax',
        dataType: "json",
        data:{
            "test_suite": thisSuite,
            "test_type": thisType,
            "test_module": thisModule,
            "test_name": thisTest
        },
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           thisButton.parent().parent().parent().next().children('div > .alert-info').addClass('hidden')
           thisButton.parent().parent().parent().next().children('div > .alert-danger').removeClass('hidden')
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

// Get last test result
function getLastTestResult(thisButton, callback) {
    thisSuite = thisButton.attr("suite")
    thisType = thisButton.attr("type")
    thisModule = thisButton.attr("category")
    thisTest = thisButton.attr("test")
    console.log(thisTest)
    $.ajax({
        type: 'POST',
        url:  '/getLastTestResult',
        dataType: "json",
        data:{
            "test_suite": thisSuite,
            "test_type": thisType,
            "test_module": thisModule,
            "test_name": thisTest
        },
        success: function(data)
        {
            if( callback ) callback(data);
        },
        error: function(err)
        {
           console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}

function getRallyInstalledModules(callback){
    $.ajax({
        type: 'POST',
        url: 'getRallyInstalledModules',
        dataType: 'json',
        success: function(data){
            if (callback) callback(data);
        },
        error: function(err){
            console.log("Παρουσιάστηκε σφάλμα!");
        }
    });
}