
// Get available Network Services stored in DB
function getServices(callback) {
    $.ajax({
        type: 'POST',
        url:  '/getNSCServices',
        dataType: "json",
        success: function(data)
        {        
            if( callback ) callback(data);
        },
        error: function(err)
        {
               console.log("Παρουσιάστηκε σφάλμα!");
        }
    }); 
}




// add Node to DB
function addNodeToDB(node,callback) {
    $.ajax({
        type:     'POST',
        url:      '/addNode',
        dataType: "json",
        data:      {"jsonIn":JSON.stringify(node)},
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}



// update Node to DB
function updateNodeToDB(node,callback) {
    $.ajax({
        type:     'POST',
        url:      '/updateNode',
        dataType: "json",
        data:      {"jsonIn":JSON.stringify(node)},
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}



// delete Node to DB
function deleteNodeToDB(node,callback) {
    $.ajax({
        type:     'POST',
        url:      '/deleteNode',
        dataType: "json",
        data:      {"jsonIn":JSON.stringify(node)},
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}



// get Nodes from DB
function getNodesFromDB(callback) {
    $.ajax({
        type:     'GET',
        url:      '/getNodes',
        dataType: "json",
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}




// add Link to DB
function addLinkToDB(link,callback) {
    $.ajax({
        type:     'POST',
        url:      '/addLink',
        dataType: "json",
        data:      {"jsonIn":JSON.stringify(link)},
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}




// delete Link to DB
function deleteLinkToDB(link,callback) {
    $.ajax({
        type:     'POST',
        url:      '/deleteLink',
        dataType: "json",
        data:      {"jsonIn":JSON.stringify(link)},
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}



// get Links from DB
function getLinksFromDB(callback) {
    $.ajax({
        type:     'GET',
        url:      '/getLinks',
        dataType: "json",
        success: function(data)
        {        
            if( callback ) callback(data);
            //console.log(data);
        },
        error: function(err)
        {
           console.log(err);
        }
    });
}