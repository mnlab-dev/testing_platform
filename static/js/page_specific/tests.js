function checkRallyInstalledModules() {

    function removeTestButtonFunctionality(module_name){
        missing_module_test_button = $("button[suite='rally'][category='"+module_name+"']")

        missing_module_test_button.parent().children().prop('disabled', true);
        missing_module_test_button.parent().children().on("click", function(e){
            e.preventDefault();
        });
        missing_module_test_button.parent().children().removeClass("btn-success").removeClass("btn-primary");
        missing_module_test_button.parent().parent().parent().parent().parent().parent().siblings().append("- Module not installed.");
    }

    getRallyInstalledModules(function(data){
        if (data.ceilometer == "False"){
            removeTestButtonFunctionality("ceilometer");
        }
        if (data.cinder == "False"){
            removeTestButtonFunctionality("cinder");
        }
        if (data.glance == "False"){
            removeTestButtonFunctionality("glance");
        }
        if (data.heat == "False"){
            removeTestButtonFunctionality("heat");
        }
        if (data.keystone == "False"){
            removeTestButtonFunctionality("keystone");
        }
        if (data.neutron == "False"){
            removeTestButtonFunctionality("neutron");
        }
        if (data.nova == "False"){
            removeTestButtonFunctionality("nova");
        }
    });


}

checkRallyInstalledModules();

function showModalLastResultRobot(thisButton){
    getLastTestResult(thisButton, function(data){
        $("#test_result_robot_label").text(thisButton.parent().parent().siblings("div").children(0).html() + " - Result");
        $('#test_result_robot_content_iframe').attr( 'src', function ( i, val ) { return val; });
        $("#test_robot_result").modal('show');
    })
}


function showModalLastResult(thisButton){
    getLastTestResult(thisButton, function(data){
        $("#test_result_rally_label").text(thisButton.parent().parent().siblings("div").children(0).html() + " - Result");
        $('#test_result_rally_content_iframe').attr( 'src', function ( i, val ) { return val; });
        $("#test_rally_result").modal('show');
    })




}


function launchTest(thisButton){

    thisButton.parent().parent().parent().next().children('div > .alert').addClass('hidden');
    thisButton.parent().parent().parent().next().children('div > .alert-info').removeClass('hidden');

    launchTestAjax(thisButton, function(test){

//        thisButton.parent().parent().next().children('div > .alert-info').addClass('hidden')
//                                                                         .text("Test done.");

        thisButton.parent().parent().parent().next().children('div > .alert-info').addClass('hidden');
        if (test["message_type"] == "error"){
            console.log(test["last_test_result"]);
            thisButton.parent().parent().parent().next().children('div > .alert-danger').removeClass('hidden')
                                                                                        .text(response["last_test_result"]);
        }
        else{
            thisButton.parent().parent().parent().next().children('div > .alert-success').removeClass('hidden')
                                                                                         .text("Test is complete.");
        }

        $("#test_result_content").text("Test results are not available yet. Please try again later.");
    });

}