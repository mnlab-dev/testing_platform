
function installTestingEnvironment(thisButton){

    thisButton.parent().parent().next().children('.alert').addClass('hidden');
    thisButton.parent().parent().next().children('.alert-info').removeClass('hidden');

    installTestingEnvironmentAjax(thisButton, function(response){

        thisButton.parent().parent().next().children('.alert-info').addClass('hidden');

        console.log(response["result"]);
        if (response["result"] === false || response["result"] === "false"){
            thisButton.parent().parent().next().children('.alert-danger').removeClass('hidden')
                                                                                .text("Installation failed.");
        }
        else{
            thisButton.parent().parent().next().children('.alert-success').removeClass('hidden')
                                                                                .text("Testing Environment installed successfully!");
        }

        $(".sidebar-item[name='tests']").removeClass("hidden");

    });

}


function checkTestingEnvironment(thisButton){

    parentDiv = thisButton.parent().parent().next()
    parentDiv.children('.alert').addClass('hidden');
    parentDiv.children('.alert-info').removeClass('hidden');


    checkTestingEnvironmentAjax(thisButton, function(response){

        parentDiv.children('.alert-info').addClass('hidden');

        console.log(response["result"]);
        if (response["result"] === false || response["result"] === "false"){
            parentDiv.children('.alert-warning').removeClass('hidden')
                                               .text("Testing Environment is not configured correctly.");
        }
        else{
            parentDiv.children('.alert-success').removeClass('hidden')
                                                .text("Testing Environment is configured correctly!");
        }
    });


}


function cleanTestingEnvironment(thisButton){

    thisButton.parent().parent().next().children('.alert').addClass('hidden');
    thisButton.parent().parent().next().children('.alert-info').removeClass('hidden');

    cleanTestingEnvironmentAjax(thisButton, function(response){

        thisButton.parent().parent().next().children('.alert-info').addClass('hidden');

        if (response["result"] === false || response["result"] === "false"){
            thisButton.parent().parent().next().children('.alert-danger').removeClass('hidden')
                                                                                .text("Cleaning failed.");
        }
        else{
            thisButton.parent().parent().next().children('.alert-success').removeClass('hidden')
                                                                                .text("Testing Environment removed successfully!");
        }


    });
}
