import json, os, sys
from configuration import config as cnf
import configparser
import sqlite3 as lite


class InstallationHelper:

    def get_deployment_id(self):
        con = lite.connect(os.path.join(cnf.RALLY_DATABASE_DIR, cnf.RALLY_DATABASE_NAME))

        with con:
            cur = con.cursor()
            cur.execute("select uuid from deployments where name = ? order by updated_at desc limit 1;",
                        (cnf.DEPLOYMENT_NAME,))
            deployment_id = str(cur.fetchone()[0])
            return deployment_id

    def replace_floating_network(self, floating_network_name, scenario_file):
        with open(scenario_file, 'r+') as f:
            try:
                clean_json_string = ""
                latest_new_line_char = 0
                # in case json has template-like "{% %}" lines
                for line in f:
                    if not line.startswith("{%"):
                        clean_json_string += line + "\n"
                    else:
                        latest_new_line_char += line.find("\n") + 1

                data = json.loads(clean_json_string)
                test_name = data.keys()[0]
                if 'args' in data[test_name][0]:
                    if 'floating_network' in data[test_name][0]["args"]:
                        print "FILE MODIFIED: " + scenario_file
                        data[test_name][0]["args"]["floating_network"] = floating_network_name
                        f.seek(latest_new_line_char)
                        json.dump(data, f, indent=4)
                        f.truncate()
                else:
                    print "floating_network keyname not found"
            except Exception as e:
                print scenario_file
                print e
                print sys.exc_traceback.tb_lineno

    def replace_template_path(self, template_path, scenario_file):
        with open(scenario_file, 'r+') as f:
            try:
                clean_json_string = ""
                latest_new_line_char = 0
                # in case json has template-like "{% %}" lines
                for line in f:
                    if not line.startswith("{%"):
                        clean_json_string += line + "\n"
                    else:
                        latest_new_line_char += line.find("\n") + 1

                data = json.loads(clean_json_string)
                test_name = data.keys()[0]
                if 'args' in data[test_name][0]:
                    if 'template_path' in data[test_name][0]["args"]:
                        print "FILE MODIFIED: " + scenario_file
                        data[test_name][0]["args"]["template_path"] = os.path.join(template_path, data[test_name][0]["args"]["template_path"])
                        f.seek(latest_new_line_char)
                        json.dump(data, f, indent=4)
                        f.truncate()
                else:
                    print "template_path keyname not found"
            except Exception as e:
                print scenario_file
                print e
                print sys.exc_traceback.tb_lineno

    def replace_updated_template_path(self, updated_template_path, scenario_file):
        with open(scenario_file, 'r+') as f:
            try:
                clean_json_string = ""
                latest_new_line_char = 0
                # in case json has template-like "{% %}" lines
                for line in f:
                    if not line.startswith("{%"):
                        clean_json_string += line + "\n"
                    else:
                        latest_new_line_char += line.find("\n") + 1

                data = json.loads(clean_json_string)
                test_name = data.keys()[0]
                if 'args' in data[test_name][0]:
                    if 'updated_template_path' in data[test_name][0]["args"]:
                        print "FILE MODIFIED: " + scenario_file
                        data[test_name][0]["args"]["updated_template_path"] = os.path.join(updated_template_path, data[test_name][0]["args"]["updated_template_path"])
                        f.seek(latest_new_line_char)
                        json.dump(data, f, indent=4)
                        f.truncate()
                else:
                    print "template_path keyname not found"
            except Exception as e:
                print scenario_file
                print e
                print sys.exc_traceback.tb_lineno

    def replace_cirros_image(self, cirros_name, scenario_file):
        with open(scenario_file, 'r+') as f:
            try:
                clean_json_string = ""
                latest_new_line_char = 0
                # in case json has template-like "{% %}" lines
                for line in f:
                    if not line.startswith("{%"):
                        clean_json_string += line + "\n"
                    else:
                        latest_new_line_char += line.find("\n") + 1

                data = json.loads(clean_json_string)
                test_name = data.keys()[0]
                if 'args' in data[test_name][0]:
                    if 'image' in data[test_name][0]["args"]:
                        if 'name' in data[test_name][0]["args"]["image"]:
                            print "FILE MODIFIED: " + scenario_file
                            data[test_name][0]["args"]["image"]["name"] = cirros_name
                            f.seek(latest_new_line_char)
                            json.dump(data, f, indent=4)
                            f.truncate()
                else:
                    print "image > name keyname not found"
            except Exception as e:
                print scenario_file
                print e
                print sys.exc_traceback.tb_lineno

    def replace_heat_yaml(self, cirros_name, public_network_name, yaml_heat_file):
        with open(yaml_heat_file, 'r+') as f:
            try:
                clean_yaml_string = ""
                latest_new_line_char = 0
                # in case json has template-like "{% %}" lines
                for line in f:
                    start_image = line.find("cirros")
                    end_image = line.find("uec") + 3

                    start_public_network = line.find("default: public")

                    if start_image != -1:
                        clean_yaml_string += line.replace(line[start_image:end_image], cirros_name) + "\n"
                    elif start_public_network != -1:
                        clean_yaml_string += line.replace("default: public", "default: " + public_network_name) + "\n"
                    else:
                        clean_yaml_string += line



                print clean_yaml_string
                f.seek(0)
                f.truncate()
                f.write(clean_yaml_string)

            except Exception as e:
                print yaml_heat_file
                print e
                print sys.exc_traceback.tb_lineno

    def find_rally_json_scenarios(self, outer_path):

        # outer_path = "/root"
        print outer_path
        f = []
        for dirname in os.listdir(outer_path):
            if os.path.isdir(os.path.join(outer_path, dirname)):
                for filename in os.listdir(os.path.join(outer_path, dirname)):
                    if os.path.isfile(os.path.join(outer_path, dirname, filename)) and filename.split(".")[-1] == "json":
                        f.append(os.path.join(outer_path, dirname, filename))
        return f

    def correct_rally_files(self, rally_test_dir, public_network_name, template_path, cirros_name, yaml_file):

        json_files = self.find_rally_json_scenarios(rally_test_dir)

        for json_file in json_files:
            self.replace_floating_network(public_network_name, json_file)
            self.replace_template_path(template_path, json_file)
            self.replace_updated_template_path(template_path, json_file)
            self.replace_cirros_image(cirros_name, json_file)

        # TODO create more generic function
        self.replace_heat_yaml(cirros_name, public_network_name, os.path.join(template_path, "templates", yaml_file))

    def modify_openstack_rc_json(self, rc_json_file):
        with open(rc_json_file, 'w+') as f:
            data = {
                        "type": "ExistingCloud",
                        "auth_url": cnf.OS_AUTH_URL,
                        "admin": {
                            "username":  cnf.OS_USERNAME,
                            "password": cnf.OS_PASSWORD,
                            "tenant_name": cnf.OS_TENANT_NAME
                        }
                    }

            json.dump(data, f, indent=4)

    def correct_tempest_files(self):
        deployment_id = self.get_deployment_id()
        tempest_conf_file_dir = os.path.join(cnf.BASE_USER_DIR, ".rally", "tempest", "for-deployment-" + deployment_id, "tempest.conf")
        config = configparser.ConfigParser()
        # Writing our configuration file to 'tempest.conf'
        config.read(tempest_conf_file_dir)
        config.set('identity', 'region', "regionOne")
        config.set('validation', 'run_validation', "False")
        with open(tempest_conf_file_dir, 'w+') as configfile:
            config.write(configfile)

