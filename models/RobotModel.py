import logging
import sqlite3 as lite
import os
from configuration import config as cnf
from helpers.CommandExecutionHelper import CommandExecutionHelper
from models import TestModel

test_modules = ["ceilometer", "cinder", "glance", "heat", "keystone", "neutron", "nova", "authenticate", "vm", "quotas"]

MODE = "DEBUG"
# MODE = "INFO"



""" logging configuration """
logger = logging.getLogger('config_functest')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
if MODE == "DEBUG":
    ch.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Robot:

    def __init__(self, state=None):
        self.state = state

    def install(self):
        """
        Install Robot Framework, OVS, Mininet
        """
        command_1 = "sudo -S bash "+os.path.join(cnf.ROBOT_REPO_DIR, "install_robot.sh")
        command_list = [command_1]
        cmd_help = CommandExecutionHelper()
        (stdout, stderr) = cmd_help.command_execution(command_list)

        print stderr
        if len(stderr) > 1:
            print "CASE ERROR!"
            response = stderr
        else:
            print "CASE SUCCESS!"
            response = stdout
        return response


    def check(self):
        """
        Check if Robot is installed and properly configured
        """

        response = True

        # Check Robot
        command_1 = os.path.join(cnf.BASE_USER_DIR, "robot_virtualenv", "bin", "pybot")
        command_list = [command_1]
        cmd_help = CommandExecutionHelper()
        (stdout, stderr) = cmd_help.command_execution(command_list)
        print stderr
        if stderr != "":
            logger.debug("   Robot is NOT installed")
            response = False

        # Check OVS
        command_1 = "ovs-vswitchd --version"
        command_list = [command_1]
        cmd_help = CommandExecutionHelper()
        (stdout, stderr) = cmd_help.command_execution(command_list)
        print stderr
        if stderr != "":
            logger.debug("   OpenVSwitch is NOT installed")
            response = False


        # Check Mninet
        command_1 = "mn --version"
        command_list = [command_1]
        cmd_help = CommandExecutionHelper()
        (stdout, stderr) = cmd_help.command_execution(command_list)
        print stderr
        if stderr != "":
            logger.debug("   Mininet is NOT installed")
            response = False

        return response



    def clean(self):
        """
        Clean robot, mininet, ovs
        """
        pass


    def launch_test(self, test_module, test_type, test_name):
        """
        Clean the existing functest environment
        """
        logger.info("Test Type: " + test_type)  # opendaylight or tnova-component
        logger.info("Module Name: " + test_module) # openflowplugin/Flows_OF13/
        logger.info("Test Name: " + test_name) # 300__dst_ip.robot

        command_1 = None
        if test_type == "opendaylight":
        # First command - choosing deployment by id
            command_1 = os.path.join(cnf.BASE_USER_DIR, "robot_virtualenv", "bin", "pybot") + " -v CONTROLLER:" + cnf.ODL_IP + " " + \
                               os.path.join(cnf.ODL_BASE_TEST_DIR, test_module, test_name)
        else:
            command_1 = os.path.join(cnf.BASE_USER_DIR, "robot_virtualenv", "bin", "pybot") + " " + \
                        os.path.join(cnf.BASE_DIR, "robot_tests", test_module, test_name)


        command_list = [command_1]

        cmd_help = CommandExecutionHelper()
        (stdout, stderr) = cmd_help.command_execution(command_list)
        print "---------------------------- stdout --------------------------------"
        # print stdout
        print "---------------------------- stdout end --------------------------------"
        # print "---------------------------- between --------------------------------"
        output_report_html = os.path.join("report.html")
        output_log_html = os.path.join("log.html")
        # xml_string_from_file = "Test failed!"

        # read text from file
        f = file(output_report_html)
        xml_string_from_report_html = f.read()
        f = file(output_log_html)
        xml_string_from_log_html = f.read()
        # print "---------------------------- print html --------------------------------"
        print xml_string_from_report_html
        # print "---------------------------- end print html --------------------------------"

        test = TestModel.TestModel(test_suite="robot", test_name=test_name, exists=True)
        # print stderr
        if len(stderr) > 1:
            print "CASE ERROR!"
            # {% raw %} {% endraw %} used to fix "{{", "}}" delimiter conflict between jinja2 and angularjs
            # test.report_html = stderr
            # test.message_type = 'error'
        else:
            print "CASE SUCCESS!"

        # Originally was indented under above "else:"
        if xml_string_from_report_html and xml_string_from_log_html:
            test.report_html = "{% raw %}" + xml_string_from_report_html + "{% endraw %}"
            test.log_html = "{% raw %}" + xml_string_from_log_html + "{% endraw %}"
        else:
            test.report_html = stdout
        test.message_type = 'success'
        # end indentation

        return test
