import logging
import sqlite3 as lite
import os
from configuration import config as cnf
from helpers.CommandExecutionHelper import CommandExecutionHelper
from models import TestModel

test_modules = ["ceilometer", "cinder", "glance", "heat", "keystone", "neutron", "nova", "authenticate", "vm", "quotas"]

MODE = "DEBUG"
# MODE = "INFO"



""" logging configuration """
logger = logging.getLogger('config_functest')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
if MODE == "DEBUG":
    ch.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Tempest:

    def __init__(self, state = None):
        self.state = state

    def install(self):
        """
        Currently it gets installed with rally, it could be configured to be installed seperately.
        """
        pass


    def check(self):
        """
        Check if Tempest is installed and properly configured
        """
        pass

    def launch_test(self, test_module, test_name):
        """
        Launch tempest test
        """
        logger.info("Module Name: " + test_module)
        logger.info("Test Name: " + test_name)

        def get_verification_id():
            con = lite.connect(os.path.join(cnf.RALLY_DATABASE_DIR, cnf.RALLY_DATABASE_NAME))

            with con:

                con.row_factory = lite.Row

                cur = con.cursor()
                sql_statement = "select uuid from verifications where set_name = '" + test_name + "' order by created_at desc limit 1;"
                cur.execute(sql_statement)
                row = cur.fetchone()
                return row["uuid"]

        command_1 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify start --deployment " + cnf.DEPLOYMENT_NAME + " --set " + test_name
        # command_2 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify results --html --output_file tempest.html"
        cmd_help = CommandExecutionHelper()
        (stdout, stderr) = cmd_help.command_execution([command_1])
        # cmd_help.command_execution_communicate([command_3], "y")

        output_file = os.path.join("tempest.html")
        xml_string_from_file = "Test failed!"
        try:
            verification_id = get_verification_id()
            # print "---------------------------- between end --------------------------------"

            command_3 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify results --html " + verification_id + " --out " + output_file
            (stdout, stderr) = cmd_help.command_execution([command_3])
            # read text from file
            f = file(output_file)
            xml_string_from_file = f.read()
            # print "---------------------------- print html --------------------------------"
            # print xml_string_from_file
            # print "---------------------------- end print html --------------------------------"
        except Exception, e:
            stderr = e[0]




        # print "---------------------------- print err --------------------------------"
        # print stderr
        # print "---------------------------- end print err --------------------------------"
        test = TestModel.TestModel(test_suite="tempest", test_name=test_name, exists=True)
        print stderr
        if type(stderr) != int:
            if len(stderr) > 1:
                print "CASE ERROR!"
                # {% raw %} {% endraw %} used to fix "{{", "}}" delimiter conflict between jinja2 and angularjs
                test.last_test_result = stderr
                test.message_type = 'error'
            else:
                print "CASE SUCCESS!"
                if xml_string_from_file:
                    test.last_test_result = "{% raw %}" + xml_string_from_file + "{% endraw %}"
                else:
                    test.last_test_result = stdout
                test.message_type = 'success'

        return test
