import sqlite3 as lite
import os
from configuration import config as cnf
from helpers.CommandExecutionHelper import CommandExecutionHelper
import logging

MODE = "DEBUG"
# MODE = "INFO"



""" logging configuration """
logger = logging.getLogger('config_functest')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
if MODE == "DEBUG":
    ch.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class TestingEnvironment:

    def clean(self):
        """
        Clean the existing testing environment
        """
        logger.info("Removing current testing environment...")
        if os.path.exists(cnf.RALLY_INSTALLATION_DIR):
            logger.debug("Removing Rally installation directory %s" % cnf.RALLY_INSTALLATION_DIR)
            bash_argument_1 = cnf.ROOT_PASSWORD
            command_1 = "sudo -S bash "+os.path.join(cnf.RALLY_REPO_DIR, "delete_testing_environment.sh") + " " + bash_argument_1
            command_list = [command_1]
            cmd_help = CommandExecutionHelper()
            (stdout, stderr) = cmd_help.command_execution(command_list)

            print stderr
            if len(stderr) > 1:
                print "CASE ERROR!"
                response = False
            else:
                print "CASE SUCCESS!"
                response = True
            return response