import logging
import configparser, json
import sqlite3 as lite
import os
from configuration import config as cnf
from helpers.CommandExecutionHelper import CommandExecutionHelper
from helpers.InstallationHelper import InstallationHelper
from models import TestModel


test_modules = ["ceilometer", "cinder", "glance", "heat", "keystone", "neutron", "nova", "authenticate", "vm", "quotas"]

MODE = "DEBUG"
# MODE = "INFO"



""" logging configuration """
logger = logging.getLogger('config_functest')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
if MODE == "DEBUG":
    ch.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Rally:

    def __init__(self, state = None):
        self.state = state

    def install(self):
        install_help = InstallationHelper()
        install_help.modify_openstack_rc_json(os.path.join(cnf.BASE_DIR + "/rc.json"))
        # command_5 = "sudo -S find / -name \"argparse*\" -exec rm  -rf {} \;"
        # command_5_2 = "sudo -S pip install argparse"

        bash_argument_1 = cnf.ROOT_PASSWORD
        command_0 = "bash "+os.path.join(cnf.RALLY_REPO_DIR, "tempArgparseFix.sh") + " " + bash_argument_1

        command_1 = "sudo -S apt-get update"
        command_2 = "sudo -S apt-get -y install libssl-dev libffi-dev python-dev libxml2-dev libxslt1-dev libpq-dev git build-essential python-virtualenv"
        command_3 = "bash  " + os.path.join(cnf.RALLY_REPO_DIR, "install_rally.sh")
        command_4 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " deployment create --name t-nova-cloud --filename " + os.path.join(cnf.BASE_DIR + "/rc.json")



        command_5 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify install"
        command_6 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify showconfig"
        cmd_help = CommandExecutionHelper()
        cmd_help.command_execution([command_0])
        cmd_help.command_execution([command_1, command_2])
        cmd_help.command_execution_communicate([command_3], "y")
        cmd_help.command_execution([command_4])

        cmd_help.command_execution_communicate([command_5], "y")
        (stdout, stderr) = cmd_help.command_execution_communicate([command_6], "y")
        install_help.correct_rally_files(cnf.RALLY_TESTS_DIR, cnf.OS_PUBLIC_NET_NAME, cnf.RALLY_HEAT_TEMPLATE_DIR, cnf.OS_CIRROS_IMG_NAME, "server-with-ports.yaml.template")
        # install_help.correct_tempest_files()

        print stderr
        if len(stderr) > 1:
            print "CASE ERROR!"
            response = stderr
        else:
            print "CASE SUCCESS!"
            response = stdout
        return response


    def check(self):
        """
        Check if Rally is installed and properly configured
        """
        print "********** RALLY_INSTALLATION_DIR ******************"
        print cnf.RALLY_INSTALLATION_DIR
        print os.path.exists(cnf.RALLY_INSTALLATION_DIR)
        if os.path.exists(cnf.RALLY_INSTALLATION_DIR):
            logger.debug("   Rally installation directory found in %s" % cnf.RALLY_INSTALLATION_DIR)
            command_1 = "source "+os.path.join(cnf.BASE_DIR, "bin", "activate")
            command_2 = "rally deployment list | grep "+cnf.DEPLOYMENT_NAME
            command_3 = "deactivate"
            command_list = [command_1, command_2, command_3]
            cmd_help = CommandExecutionHelper()
            (stdout, stderr) = cmd_help.command_execution(command_list)
            #if the command does not exist or there is no deployment
            line = stdout
            if line == "":
                logger.debug("   Rally deployment NOT found")
                return False
            logger.debug("   Rally deployment found")
            self.state = "Rally is installed"
            return True
        else:
            self.state = "Rally is not installed"
            return False


    def clean(self):
        """
        Clean the existing functest environment
        """
        logger.info("Removing current functest environment...")
        if os.path.exists(cnf.RALLY_INSTALLATION_DIR):
            logger.debug("Removing Rally installation directory %s" % cnf.RALLY_INSTALLATION_DIR)
            # shutil.rmtree(cnf.RALLY_INSTALLATION_DIR)
            # command_1 = "sudo -S bash "+os.path.join(cnf.RALLY_REPO_DIR, "delete_rally.sh")
            # command_list = [command_1]
            command_1 = "bash "+os.path.join(cnf.RALLY_REPO_DIR, "delete_rally.sh")
            command_list = [command_1]
            cmd_help = CommandExecutionHelper()
            (stdout, stderr) = cmd_help.command_execution(command_list)

            print stderr
            if len(stderr) > 1:
                print "CASE ERROR!"
                response = False
            else:
                print "CASE SUCCESS!"
                response = True
            return response


    def launch_test(self, test_module, test_name):
        """
        Clean the existing functest environment
        """
        logger.info("Module Name: " + test_module)
        logger.info("Test Name: " + test_name)

        def get_task_id():
            # def find_between( s, first, last ):
            #     try:
            #         start = s.index( first ) + len( first )
            #         end = s.index( last, start )
            #         return s[start:end]
            #     except ValueError:
            #         return ""
            #
            # return find_between(stdout, "rally task report ", " --out output.html")

            con = lite.connect(os.path.join(cnf.RALLY_DATABASE_DIR, cnf.RALLY_DATABASE_NAME))

            with con:

                con.row_factory = lite.Row

                cur = con.cursor()
                sql_statement = "select uuid from tasks order by created_at desc limit 1;"
                cur.execute(sql_statement)
                row = cur.fetchone()
                return row["uuid"]

        # First command - choosing deployment by id
        # TODO change sudo to venv usage
        # command_1 = "sudo -S rally deployment use " + cnf.DEPLOYMENT_NAME
        # command_2 = "sudo -S rally task start " + cnf.RALLY_TESTS_DIR + "/" + test_module + "/" + test_name + ".json"
        bash_argument_1 = cnf.DEPLOYMENT_NAME
        bash_argument_2 = cnf.RALLY_TESTS_DIR + "/" + test_module + "/" + test_name + ".json"
        command_1 = "bash " + os.path.join(cnf.RALLY_REPO_DIR, "rally_run_test.sh") + " " + bash_argument_1 + " " + bash_argument_2
        command_list = [command_1]

        cmd_help = CommandExecutionHelper()
        cmd_help.command_execution(command_list)
        # print "---------------------------- stdout --------------------------------"
        # print stdout
        # print "---------------------------- stdout end --------------------------------"
        # print "---------------------------- between --------------------------------"
        output_file = os.path.join("output.html")
        xml_string_from_file = "Test failed!"
        try:
            task_id = get_task_id()
            # print "---------------------------- between end --------------------------------"

            command_3 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " task report " + task_id + " --out " + output_file
            # print "---------------------------- command --------------------------------"
            # print command_3
            # print "---------------------------- command end --------------------------------"
            (stdout, stderr) = cmd_help.command_execution([command_3])
            # read text from file
            f = file(output_file)
            xml_string_from_file = f.read()
            # print "---------------------------- print html --------------------------------"
            # print xml_string_from_file
            # print "---------------------------- end print html --------------------------------"
        except Exception, e:
            stderr = e[0]




        # print "---------------------------- print err --------------------------------"
        # print stderr
        # print "---------------------------- end print err --------------------------------"
        test = TestModel.TestModel(test_suite="rally", test_name=test_name, exists=True)
        print stderr
        if type(stderr) != int:
            if len(stderr) > 1:
                print "CASE ERROR!"
                # {% raw %} {% endraw %} used to fix "{{", "}}" delimiter conflict between jinja2 and angularjs
                test.last_test_result = stderr
                test.message_type = 'error'
            else:
                print "CASE SUCCESS!"
                if xml_string_from_file:
                    test.last_test_result = "{% raw %}" + xml_string_from_file + "{% endraw %}"
                else:
                    test.last_test_result = stdout
                test.message_type = 'success'

        return test

    def get_installed_modules(self):
        install_help = InstallationHelper()
        deployment_id = install_help.get_deployment_id()
        config = configparser.ConfigParser()
        tempest_conf_file_dir = os.path.join(cnf.BASE_USER_DIR, ".rally", "tempest", "for-deployment-" + deployment_id, "tempest.conf")
        print tempest_conf_file_dir
        config.read(tempest_conf_file_dir)

        service_state_list = {
            "ceilometer": config["service_available"]["ceilometer"],
            "cinder": config["service_available"]["cinder"],
            "glance": config["service_available"]["glance"],
            "heat": config["service_available"]["heat"],
            "neutron": config["service_available"]["neutron"],
            "nova": config["service_available"]["nova"],
            "sahara": config["service_available"]["sahara"],
            "swift": config["service_available"]["swift"],
            "horizon": config["service_available"]["horizon"]
        }

        return json.dumps(service_state_list)