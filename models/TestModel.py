import sqlite3 as lite
import getpass
import os
from configuration import config as cnf


class TestModel:

    def __init__(self, test_suite=None, test_name=None, test_type=None, test_module=None, last_test_result=None,
                 report_html=None, log_html=None, exists=False, message_type="info"):
        self.test_name = test_name
        self.test_type = test_type
        self.test_module = test_module
        self.test_suite = test_suite
        self.last_test_result = last_test_result
        self.report_html = report_html
        self.log_html = log_html
        self.exists = exists
        self.message_type = message_type

    def create_tp_database(self):
        con = lite.connect(os.path.join(cnf.DATABASE_DIRECTORY, cnf.DATABASE_NAME))

        with con:

            con.row_factory = lite.Row
            cur = con.cursor()
            # Create Rally Test result table
            sql = 'create table if not exists rally_tests (test_name char(50) PRIMARY KEY,' \
                                                          'last_test_result text );'
            cur.execute(sql)

            # Create Robot Test result table
            sql = 'create table if not exists robot_tests (test_name char(50) PRIMARY KEY,' \
                                                          'report_html text,' \
                                                          'log_html text );'
            cur.execute(sql)

            # Create Robot Test result table
            sql = 'create table if not exists tempest_tests (test_name char(50) PRIMARY KEY,' \
                                                          'last_test_result text );'
            cur.execute(sql)

    def getTest(self):
        print os.path.join(cnf.DATABASE_DIRECTORY, cnf.DATABASE_NAME)
        con = lite.connect(os.path.join(cnf.DATABASE_DIRECTORY, cnf.DATABASE_NAME))

        with con:

            con.row_factory = lite.Row

            cur = con.cursor()
            if self.test_suite == "rally":
                sql_statement = "SELECT test_name, last_test_result FROM rally_tests where test_name = '" + str(self.test_name) + "';"
                cur.execute(sql_statement)
                row = cur.fetchone()
                if row is not None:
                    self.exists = True
                    self.last_test_result = row["last_test_result"]

            elif self.test_suite == "robot":
                sql_statement = "SELECT test_name, report_html, log_html FROM robot_tests where test_name = '" + str(self.test_name) + "';"
                cur.execute(sql_statement)
                row = cur.fetchone()
                if row is not None:
                    self.exists = True
                    self.report_html = row["report_html"]
                    self.log_html = row["log_html"]

            elif self.test_suite == "tempest":
                sql_statement = "SELECT test_name, last_test_result FROM tempest_tests where test_name = '" + str(self.test_name) + "';"
                cur.execute(sql_statement)
                row = cur.fetchone()
                if row is not None:
                    self.exists = True
                    self.last_test_result = row["last_test_result"]


    def insertTest(self):
        print os.path.join(cnf.DATABASE_DIRECTORY, cnf.DATABASE_NAME)
        con = lite.connect(os.path.join(cnf.DATABASE_DIRECTORY, cnf.DATABASE_NAME))

        with con:

            con.row_factory = lite.Row

            cur = con.cursor()

            if self.test_suite == "rally":
                cur.execute("SELECT test_name, last_test_result FROM rally_tests where test_name = '" + str(self.test_name) + "';")
                row = cur.fetchone()
                if self.test_name is not None and self.last_test_result is not None:
                    if row is None:
                        self.exists = False
                        sql_statement = "insert into rally_tests values(?,?);"
                        cur.execute(sql_statement, (str(self.test_name), str(self.last_test_result),))
                        self.exists = True
                    else:
                        self.exists = True
                        sql_statement = "update rally_tests set last_test_result=? where test_name=? ;"
                        cur.execute(sql_statement, (str(self.last_test_result), str(self.test_name),))

            elif self.test_suite == "robot":
                cur.execute("SELECT test_name, report_html, log_html FROM robot_tests where test_name = '" + str(self.test_name) + "';")
                row = cur.fetchone()
                if self.test_name is not None and self.report_html is not None and self.log_html is not None:
                    if row is None:
                        self.exists = False
                        sql_statement = "insert into robot_tests values(?,?,?);"
                        cur.execute(sql_statement, (str(self.test_name), str(self.report_html), str(self.log_html),))
                        self.exists = True
                    else:
                        self.exists = True
                        sql_statement = "update robot_tests set report_html=?, log_html=? where test_name=? ;"
                        cur.execute(sql_statement, (str(self.report_html), str(self.log_html), str(self.test_name),))

            if self.test_suite == "tempest":
                cur.execute("SELECT test_name, last_test_result FROM tempest_tests where test_name = '" + str(self.test_name) + "';")
                row = cur.fetchone()
                if self.test_name is not None and self.last_test_result is not None:
                    if row is None:
                        self.exists = False
                        sql_statement = "insert into tempest_tests values(?,?);"
                        cur.execute(sql_statement, (str(self.test_name), str(self.last_test_result),))
                        self.exists = True
                    else:
                        self.exists = True
                        sql_statement = "update tempest_tests set last_test_result=? where test_name=? ;"
                        cur.execute(sql_statement, (str(self.last_test_result), str(self.test_name),))
