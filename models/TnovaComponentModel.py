import logging
import configparser, json
import sqlite3 as lite
import os
from configuration import config as cnf
from helpers.CommandExecutionHelper import CommandExecutionHelper
from helpers.InstallationHelper import InstallationHelper
from models import TestModel


test_modules = ["ceilometer", "cinder", "glance", "heat", "keystone", "neutron", "nova", "authenticate", "vm", "quotas"]

MODE = "DEBUG"
# MODE = "INFO"



""" logging configuration """
logger = logging.getLogger('config_functest')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
if MODE == "DEBUG":
    ch.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Rally:

    def __init__(self, state = None):
        self.state = state

    def install(self):
        install_help = InstallationHelper()
        install_help.modify_openstack_rc_json(os.path.join(cnf.BASE_DIR + "/rc.json"))
        # command_5 = "sudo -S find / -name \"argparse*\" -exec rm  -rf {} \;"
        # command_5_2 = "sudo -S pip install argparse"

        bash_argument_1 = cnf.ROOT_PASSWORD
        command_0 = "bash "+os.path.join(cnf.RALLY_REPO_DIR, "tempArgparseFix.sh") + " " + bash_argument_1

        command_1 = "sudo -S apt-get update"
        command_2 = "sudo -S apt-get -y install libssl-dev libffi-dev python-dev libxml2-dev libxslt1-dev libpq-dev git build-essential python-virtualenv"
        command_3 = "bash  " + os.path.join(cnf.RALLY_REPO_DIR, "install_rally.sh")
        command_4 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " deployment create --name t-nova-cloud --filename " + os.path.join(cnf.BASE_DIR + "/rc.json")



        command_5 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify install"
        command_6 = os.path.join(cnf.BASE_USER_DIR + "/rally/bin/rally") + " verify showconfig"
        cmd_help = CommandExecutionHelper()
        cmd_help.command_execution([command_0])
        cmd_help.command_execution([command_1, command_2])
        cmd_help.command_execution_communicate([command_3], "y")
        cmd_help.command_execution([command_4])

        cmd_help.command_execution_communicate([command_5], "y")
        (stdout, stderr) = cmd_help.command_execution_communicate([command_6], "y")
        install_help.correct_rally_files(cnf.RALLY_TESTS_DIR, cnf.OS_PUBLIC_NET_NAME, cnf.RALLY_HEAT_TEMPLATE_DIR, cnf.OS_CIRROS_IMG_NAME, "server_with_ports.yaml.template")
        install_help.correct_tempest_files()

        print stderr
        if len(stderr) > 1:
            print "CASE ERROR!"
            response = stderr
        else:
            print "CASE SUCCESS!"
            response = stdout
        return response

