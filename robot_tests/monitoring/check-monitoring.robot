*** Settings ***
Documentation  Test suite for accessing the VIM Monitoring Backend
Library        Collections
Library        RequestsLibrary


*** Variables ***
${MONITORING_HOST}  10.143.0.168:8080
${UUID}  ef65aea2-80c8-4227-96db-ff32d4ce5ab9
#${VPROXY_UUID}  6467a87b-5a7a-46b0-8449-7862b4d901a4
${TC_UUID}  ef65aea2-80c8-4227-96db-ff32d4ce5ab9

*** Test Cases ***
Get the latest value of CPU utilisation
    Get Measurement  generic  cpu_util  ${UUID}

Get the latest value of idle CPU usage
    Get Measurement  generic  cpuidle  ${UUID}

Get the latest root filesystem status
    Get Measurement  generic  fsfree  ${UUID}

Get the latest value of load average of the past fifteen minutes
    Get Measurement  generic  load_longterm  ${UUID}

Get the latest value of load average of the past five minutes
    Get Measurement  generic  load_midterm  ${UUID}

Get the latest value of load average of the past one minute
    Get Measurement  generic  load_shortterm  ${UUID}

Get the latest value of free memory
    Get Measurement  generic  memfree  ${UUID}

Get the latest value of rate of incoming bytes
    Get Measurement  generic  network_incoming  ${UUID}

Get the latest value of rate of outgoing bytes
    Get Measurement  generic  network_outgoing  ${UUID}

Get the latest value of blocked processes
    Get Measurement  generic  processes_blocked  ${UUID}

Get the latest value of paging processes
    Get Measurement  generic  processes_blocked  ${UUID}

Get the latest value of running processes
    Get Measurement  generic  processes_running  ${UUID}

Get the latest value of sleeping processes
    Get Measurement  generic  processes_sleeping  ${UUID}

Get the latest value of stopped processes
    Get Measurement  generic  processes_stopped  ${UUID}

Get the latest value of zombie processes
    Get Measurement  generic  processes_zombie  ${UUID}

#Cache disk utilization
#    Get Measurement  vproxy  cachediskutilization  ${VPROXY_UUID}
#
#Cache memory utilization
#    Get Measurement  vproxy  cachememkutilization  ${VPROXY_UUID}
#
#CPU consumed by Squid for the last 5 minutes
#    Get Measurement  vproxy  cpuusage  ${VPROXY_UUID}
#
#Disk hits percentage for the last 5 minutes (hits that are logged as TCP_HIT)
#    Get Measurement  vproxy  diskhits  ${VPROXY_UUID}
#
#Cache hits percentage of all requests for the last 5 minutes
#    Get Measurement  vproxy  hits  ${VPROXY_UUID}
#
#Cache hits percentage of bytes sent for the last 5 minutes
#    Get Measurement  vproxy  hits_bytes  ${VPROXY_UUID}
#
#Number of HTTP requests received
#    Get Measurement  vproxy  httpnum  ${VPROXY_UUID}
#
#Memory hits percentage for the last 5 minutes (hits that are logged as TCP_MEM_HIT)
#    Get Measurement  vproxy  memoryhits  ${VPROXY_UUID}
#
#Number of users accessing the proxy
#    Get Measurement  vproxy  usernum  ${VPROXY_UUID}
#
#Bandwidth for all allowed traffic
#    Get Measurement  vtc  mbits_packets_all  ${TC_UUID}
#
#Bandwidth for Apple-related traffic
#    Get Measurement  vtc  mbits_packets_apple  ${TC_UUID}
#
#Bandwidth for BitTorrent traffic
#    Get Measurement  vtc  mbits_packets_bittorrent  ${TC_UUID}
#
#Bandwidth for DNS traffic
#    Get Measurement  vtc  mbits_packets_dns  ${TC_UUID}
#
#Bandwidth for Dropbox traffic
#    Get Measurement  vtc  mbits_packets_dropbox  ${TC_UUID}
#
#Bandwidth for Google-related traffic
#    Get Measurement  vtc  mbits_packets_google  ${TC_UUID}
#
#Bandwidth for HTTP traffic
#    Get Measurement  vtc  mbits_packets_http  ${TC_UUID}
#
#Bandwidth for iCloud traffic
#    Get Measurement  vtc  mbits_packets_icloud  ${TC_UUID}
#
#Bandwidth for Skype traffic
#    Get Measurement  vtc  mbits_packets_skype  ${TC_UUID}
#
#Bandwidth for Twitter traffic
#    Get Measurement  vtc  mbits_packets_twitter  ${TC_UUID}
#
#Bandwidth for Viber traffic
#    Get Measurement  vtc  mbits_packets_viber  ${TC_UUID}
#
#Bandwidth for Youtube traffic
#    Get Measurement  vtc  mbits_packets_youtube  ${TC_UUID}

*** Keywords ***
Get Measurement
    [Arguments]  ${tag}  ${type}  ${uuid}
    Set Tags  ${tag}
    Create Session  backend  http://${MONITORING_HOST}
    ${resp}=  Get Request  backend  /api/measurements/${uuid}.${type}
    Should Be Equal As Strings  ${resp.status_code}  200
