## Prerequisites

* sudo apt-get install python-flask
* sudo apt-get install python-configparser

## Installation

* git clone https://yan0s_ncsrd@bitbucket.org/mnlab-dev/testing_platform.git
* cd testing_platform/

###### Edit "aio.conf" file with the Openstack and Opendaylight information that apply to your installation

## Run

* python main.py

###### The web application is accessible at http://localhost:8081