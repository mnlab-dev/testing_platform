#!/bin/bash

source ~/rally/bin/activate
rally deployment create --name t-nova-cloud --filename rc.json
rally verify install
deactivate
