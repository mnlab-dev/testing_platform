#!/usr/bin/env bash

echo $1 | sudo -S find / -name "argparse*" -exec rm  -rf {} \;
sudo pip install argparse