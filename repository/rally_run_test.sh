#!/bin/bash

source ~/rally/bin/activate
rally deployment use $1
rally task start $2
deactivate
