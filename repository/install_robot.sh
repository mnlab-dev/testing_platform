#!/bin/bash

#sudo sed -i '1 i\nameserver 8.8.8.8' /etc/resolv.conf

sudo apt-get update && sudo apt-get install -y build-essential fakeroot autoconf libtool git automake autoconf gcc uml-utilities pkg-config linux-headers-`uname -r` openvswitch-switch python-qt4 python-twisted-conch

virtualenv $HOME/robot_virtualenv

source $HOME/robot_virtualenv/bin/activate
$HOME/robot_virtualenv/bin/pip install paramiko simplejson requests robotframework robotframework-sshlibrary -U robotframework-requests --upgrade robotframework-httplibrary
deactivate


# *********** OPENVSWITCH *******************

#sudo apt-get install openvswitch-switch


# ***************** MININET **************

cd

git clone git://github.com/mininet/mininet

sudo ./mininet/util/install.sh -nf


